<x-app-layout>
    <x-slot name="header">
        Yazarı Güncelle
    </x-slot>
    <p></p>
    <div class="card">
        <div class="card-body">
            <form method="POST" action="{{route('yazars.update', $yazar->id)}}">
                @method('PUT')
                @csrf
                <div class="form-group">
                    <label>Yazar Adı</label>
                    <input type="text" name="isim" class="form-control" value="{{$yazar->isim}}">
                </div>
                <div class="form-group">
                    <label>Yaş</label>
                    <input type="text" name="yas" class="form-control" value="{{$yazar->yas}}">
                </div>
                <div class="form-group">
                    <label for="">Doğum Tarihi</label>
                    <input type="datetime-local" name="dogumtarih" class="form-control" value="{{ date('Y-m-d\TH:i', strtotime($yazar->dogumtarih)) }}">
                    <br>
                </div>
                <div class="form-group">
                    <label for="">Ölüm Tarihi</label>
                    <input type="datetime-local" name="olumtarih" class="form-control" value="{{ date('Y-m-d\TH:i', strtotime($yazar->olumtarih)) }}">
                    <br>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-sm btn-block">Yazarı Düzenle</button>
                </div>
            </form>
        </div>
    </div>
</x-app-layout>