<x-app-layout>
    <x-slot name="header">
        Yazar Oluştur
    </x-slot>
    <p></p>
    <div class="card">
        <div class="card-body">
            <form action="{{route('yazars.store')}}" method="post">
                @csrf
                <div class="form-group">
                    <label>Yazar Adı Soyadı</label>
                    <input type="text" name="isim" class="form-control">
                </div>
                <div class="form-group">
                    <label>Yaş</label>
                    <input type="text" name="yas" class="form-control">
                </div>
                <div class="form-group">
                    <label>Doğum Tarihi</label>
                    <input type="datetime-local" name="dogumtarih" class="form-control">
                </div>
                <div class="form-group">
                    <label>Ölüm Tarihi</label>
                    <input type="datetime-local" name="olumtarih" class="form-control">
                </div>
                <br>

                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-sm btn-block">Kitap Oluştur</button>
                </div>
            </form>
        </div>
    </div>
</x-app-layout>