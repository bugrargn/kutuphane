<x-app-layout>
    <x-slot name="header">
        Yazarlar
    </x-slot>
    <div class="card-body">        
        <h5 class="card-title">
            <a href="{{ route('yazars.create') }}" class="btn btn-sm btn-primary">Yazar Oluştur</a>
        </h5>
        <h5 class="card-title">
        </h5>
        <table class="table table-bordered bg-white">
            <thead>
                <tr>
                    <th scope="col">Yazar</th>
                    <th scope="col">Yaş</th>
                    <th scope="col">Doğum Tarihi</th>
                    <th scope="col">Ölüm Tarihi</th>
                    <th scope="col">İşlem</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($yazars as $yazar)             
                <tr>
                    <td><a href="/admin/yazars/show/{!! $yazar->id !!}">{{$yazar->isim}}</a></td>
                    <td>{{$yazar->yas}}</td>
                    <td>{{$yazar->dogumtarih}}</td>
                    <td>{{$yazar->olumtarih}}</td>
                    <td>
                        <a href="{{route('yazars.edit', $yazar->id)}}" class="btn-sm btn-primary">Düzenle</a>
                        <a href="{{route('yazars.destroy', $yazar->id)}}" class="btn-sm btn-danger">Sil</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{$yazars->links()}}
    </div>
</x-app-layout>
