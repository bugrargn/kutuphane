<x-app-layout>
    <x-slot name="header">
        {{$yazar->isim}}
    </x-slot>
    <p></p>
    <div class="card">
        <div class="card-body">
            <a href="javascript:history.back(-1);" class="btn-sm btn-success">Geri Dön</a>
            <a href="{{route('yazars.edit', $yazar->id)}}" class="btn-sm btn-primary">Düzenle</a>
            <a href="{{route('yazars.destroy', $yazar->id)}}" class="btn-sm btn-danger">Sil</a>
            <br><br>
                <div class="form-group">
                    <label>Yaş</label>
                    <input type="text" name="yas" class="form-control" disabled="disabled" value="{{$yazar->yas}}">
                    <br>
                </div>
                <div class="form-group">
                    <label for="">Doğum Tarihi</label>
                    <input type="datetime-local" name="dogumtarih" class="form-control" disabled="disabled" value="{{ date('Y-m-d\TH:i', strtotime($yazar->dogumtarih)) }}">
                    <br>
                </div>
                <div class="form-group">
                    <label for="">Ölüm Tarihi</label>
                    <input type="datetime-local" name="olumtarih" class="form-control" disabled="disabled" value="{{ date('Y-m-d\TH:i', strtotime($yazar->olumtarih)) }}">
                    <br>
                </div>
        </div>
    </div>
</x-app-layout>