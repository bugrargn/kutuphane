<x-app-layout>
    <x-slot name="header">
        Kitabı Güncelle
    </x-slot>
    <p></p>
    <div class="card">
        <div class="card-body">
            <form method="POST" action="{{route('books.update', $book->id)}}">
                @method('PUT')
                @csrf
                <div class="form-group">
                    <label>Kitap Adı</label>
                    <input type="text" name="ad" class="form-control" value="{{$book->ad}}">
                </div>
                <div class="form-group">
                    <label>Yazar Adı</label>
                    <select class="form-control" name="yazar_id">
                        <option value="0">Yazar Seçiniz</option>
                        @foreach ($yazars as $yazar)  
                        <option value="{{$yazar->id}}">{{$yazar->isim}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Barkod</label>
                    <input type="text" name="barkod" class="form-control" value="{{$book->barkod}}">
                </div>
                <div class="form-group">
                    <label>Sayfa Sayısı</label>
                    <input type="text" name="sayfasayi" class="form-control" value="{{$book->sayfasayi}}">
                </div>
                <div class="form-group">
                    <label>Satış Fiyatı</label>
                    <input type="text" name="satisfiyat" class="form-control" value="{{$book->satisfiyat}}">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-sm btn-block">Kitabı Düzenle</button>
                </div>
            </form>
        </div>
    </div>
</x-app-layout>