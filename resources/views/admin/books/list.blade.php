<x-app-layout>
    <x-slot name="header">
        Kitaplar
    </x-slot>
    <div class="card-body">
        <h5 class="card-title">
            <a href="{{ route('books.create') }}" class="btn btn-sm btn-primary">Kitap Oluştur</a>
        </h5>
        <table class="table table-bordered bg-white">
            <thead>
                <tr>
                    <th scope="col">Kitap</th>
                    <th scope="col">Yazar</th>
                    <th scope="col">Barkod No</th>
                    <th scope="col">Sayfa Sayısı</th>
                    <th scope="col">Piyasa Satış Fiyatı</th>
                    <th scope="col">İşlem</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($books as $book)                    
                <tr>
                    <td>{{$book->ad}}</td>
                    <td><a href="/admin/yazars/show/{!! $book->yazar_id !!}">{{$book->isim}}</a></td>
                    <td>{{$book->barkod}}</td>
                    <td>{{$book->sayfasayi}}</td>
                    <td>{{$book->satisfiyat}}</td>
                    <td>
                        <a href="{{route('books.edit', $book->id)}}" class="btn-sm btn-primary">Düzenle</a>
                        <a href="{{route('books.destroy', $book->id)}}" class="btn-sm btn-danger">Sil</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{$books->links()}}
    </div>
</x-app-layout>
