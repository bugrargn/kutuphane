<x-app-layout>
    <x-slot name="header">
        Kitap Oluştur
    </x-slot>
    <p></p>
    <div class="card">
        <div class="card-body">
            <form action="{{route('books.store')}}" method="post">
                @csrf
                <div class="form-group">
                    <label>Kitap Adı</label>
                    <input type="text" name="ad" class="form-control">
                </div>
                <div class="form-group">
                    <label>Barkod No</label>
                    <input type="text" name="barkod" class="form-control">
                </div>
                <div class="form-group">
                    <label>Sayfa Sayısı</label>
                    <input type="text" name="sayfasayi" class="form-control">
                </div>
                <div class="form-group">
                    <label>Piyasa Satış Fiyatı</label>
                    <input type="text" name="satisfiyat" class="form-control">
                </div>
                <br>

                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-sm btn-block">Kitap Oluştur</button>
                </div>
            </form>
        </div>
    </div>
</x-app-layout>