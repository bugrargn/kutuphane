<x-app-layout>
    <x-slot name="header">
            Anasayfa
    </x-slot>

    <div class="alert alert-primary" role="alert">
      {{$books->count()}} kitap var
    </div>

    <div class="list-group">
        @foreach ($books as $book)
  <a href="#" class="list-group-item list-group-item-action" aria-current="true">
    <div class="d-flex w-100 justify-content-between">
      <h5 class="mb-1">{{$book->ad}}</h5>
      <small>Tarih: {{$book->created_at}}</small>
    </div>
    <p class="mb-1">Sayfa Sayısı : {{$book->sayfasayi}} Satış Fiyatı : {{$book->satisfiyat}} Barkod: {{$book->barkod}}</p>
  </a>
  @endforeach
</div>

</x-app-layout>
