<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BookMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('yazar_id')->nullable();
            $table->string('ad')->nullable();
            $table->string('barkod')->nullable();
            $table->string('sayfasayi')->nullable();
            $table->string('satisfiyat')->nullable();
            $table->timestamps();
            $table->foreign('yazar_id')->references('id')->on('yazars')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
