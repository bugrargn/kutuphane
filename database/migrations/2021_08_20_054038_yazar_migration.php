<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class YazarMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yazars', function (Blueprint $table) {
            $table->id();
            $table->string('isim');
            $table->integer('yas')->nullable();
            $table->dateTime('dogumtarih')->nullable();
            $table->dateTime('olumtarih')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('yazarlar');
    }
}
