<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Yazar extends Model
{
    use HasFactory;

    protected $fillable = ['isim', 'yas', 'dogumtarih', 'olumtarih'];

    public function books() {
        return $this->hasMany('App\Models\Book');        
    }
}
