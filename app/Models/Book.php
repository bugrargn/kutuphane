<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    protected $fillable = ['ad', 'barkod', 'sayfasayi', 'satisfiyat', 'yazar_id'];

    public function yazars() {
        return $this->belongsTo('App\Models\Yazar', Yazar::class);
    }
}
