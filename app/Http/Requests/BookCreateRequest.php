<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [            
            'ad'=>'required|min:3|max:200',
            'barkod'=>'required|min:3|max:100',
            'sayfasayi'=>'required|min:2|max:100',
            'satisfiyat'=>'required|min:2|max:100',
        ];
    }
}
