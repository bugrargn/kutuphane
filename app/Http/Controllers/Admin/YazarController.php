<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Yazar;

class YazarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $yazars = Yazar::paginate(5);
        return view('admin.yazars.list', compact('yazars'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.yazars.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Yazar::create($request->post());
        return redirect()->route('yazars.index')->withSuccess('Yeni yazar başarıyla eklendi.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $yazar = Yazar::find($id) ?? abort(404, 'Yazar bulunamadı');
        return view('admin.yazars.show', compact('yazar'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $yazar = Yazar::find($id) ?? abort(404, 'Yazar bulunamadı');
        return view('admin.yazars.edit', compact('yazar'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $yazar = Yazar::find($id) ?? abort(404, 'Yazar Bulunamadı');

        Yazar::where('id', $id)->update($request->except(['_method', '_token']));

        return redirect()->route('yazars.index')->withSuccess('Yazar Güncelleme başarıyla gerçekleşti.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $yazar = Yazar::find($id) ?? abort(404, 'Yazar Bulunamadı');
        $yazar->delete();
        return redirect()->route('yazars.index')->withSuccess('Yazar silme işlemi başarıyla gerçekleşti.');

    }
}
