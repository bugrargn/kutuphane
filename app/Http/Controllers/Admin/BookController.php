<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Book;
use App\Models\Yazar;
use App\Http\Requests\BookCreateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Yazar::join('books', 'books.yazar_id', '=', 'yazars.id')->select(['books.id', 'books.ad', 'books.yazar_id', 'books.barkod', 'books.sayfasayi', 'books.satisfiyat', 'yazars.isim'])->paginate(5);
        return view('admin.books.list', compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $books = Yazar::get();
        return view('admin.books.create', compact('books'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BookCreateRequest $request)
    {
        Book::create($request->post());
        return redirect()->route('books.index')->withSuccess('Kitap başarıyla oluşturuldu.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = Book::find($id) ?? abort(404, 'Kitap bulunamadı');
        return view('admin.books.edit', compact('book'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book = Book::find($id) ?? abort(404, 'Kitap bulunamadı');
        $yazar = DB::table('yazars')->get();
        return view('admin.books.edit', ['yazars' => $yazar], compact('book'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $book = Book::find($id) ?? abort(404, 'Kitap Bulunamadı');

        Book::where('id', $id)->update($request->except(['_method', '_token']));

        return redirect()->route('books.index')->withSuccess('Kitap Güncelleme başarıyla gerçekleşti.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Yazar::find($id) ?? abort(404, 'Kitap Bulunamadı');
        $book->delete();
        return redirect()->route('books.index')->withSuccess('Kitap silme işlemi başarıyla gerçekleşti.');
    }
}
