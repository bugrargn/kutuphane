<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;
use Illuminate\Support\Facades\DB;

class MainController extends Controller
{
    public function dashboard() {
        $books = Book::get();
        return view('dashboard', compact('books'));
    }
}
