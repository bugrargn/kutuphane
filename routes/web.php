<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;
use App\Http\Controllers\Admin\BookController;
use App\Http\Controllers\Admin\YazarController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::middleware(['auth', 'verified'])->get('/dashboard', function () {
//     return view('dashboard');
// })->name('dashboard');

Route::group(['middleware' => 'auth'], function() {
    Route::get('/dashboard', [MainController::class, 'dashboard'])->name('dashboard');
});

Route::group(['middleware' => ['auth', 'isAdmin'], 'prefix' => 'admin'], function () {
    Route::get('yazars/show/{id}', [YazarController::class, 'show'])->whereNumber('id')->name('yazars.show');
    Route::get('books/show/{id}', [BookController::class, 'show'])->whereNumber('id')->name('books.show');
    Route::get('yazars/{id}', [YazarController::class, 'destroy'])->whereNumber('id')->name('yazars.destroy');
    Route::get('books/{id}', [YazarController::class, 'destroy'])->whereNumber('id')->name('books.destroy');
    Route::resource('books', BookController::class);
    Route::resource('yazars', YazarController::class);
});